// @author laudeon
// @license MIT

'use strict'

import boom from '@hapi/boom'
import accepts from './accepts'

describe('Happy path, the client accept json', () => {
  test('Should call req.accepts', () => {
    const req = {accepts: jest.fn()}
    const res = {}
    const next = jest.fn()

    req.accepts.mockReturnValue(true)
    accepts(req, res, next)
    expect(req.accepts).toHaveBeenCalledWith('application/json')
  })

  test('Should call next without error', () => {
    const req = {accepts: jest.fn()}
    const res = {}
    const next = jest.fn()

    req.accepts.mockReturnValue(true)
    accepts(req, res, next)
    expect(next).toHaveBeenCalled()
    expect(next).toHaveBeenCalledWith()
  })
})

describe('Client does not accept json', () => {
  test('should call next with a boom 409 Not Acceptable error object', () => {
    const req = {accepts: jest.fn()}
    const res = {}
    const next = jest.fn()

    req.accepts.mockReturnValue(false)
    accepts(req, res, next)
    expect(next).toHaveBeenCalledWith(boom.notAcceptable())
  })
})

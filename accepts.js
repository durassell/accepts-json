// @author laudeon
// @license MIT

import boom from '@hapi/boom'

// App-level middleware that ensures the client accepts json as response type.
export default function acceptJSONMiddleware (req, res, next) {
  if (!req.accepts('application/json')) return next(boom.notAcceptable())
  return next()
}
